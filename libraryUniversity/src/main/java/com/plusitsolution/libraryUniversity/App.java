package com.plusitsolution.libraryUniversity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.plusitsolution.libraryUniversity.App;

@SpringBootApplication(scanBasePackages = "com.plusitsolution")
public class App 
{
    public static void main( String[] args )
    {
    	SpringApplication.run(App.class, args);
    }
}