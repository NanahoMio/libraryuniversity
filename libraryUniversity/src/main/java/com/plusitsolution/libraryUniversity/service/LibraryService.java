package com.plusitsolution.libraryUniversity.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Service;

import com.plusitsolution.libraryUniversity.domain.BookDomain;
import com.plusitsolution.libraryUniversity.domain.MemberDomain;

@Service
public class LibraryService {
	
	public Map<Integer, BookDomain> BOOK_MAP = new HashMap<>();
	public Map<Long, MemberDomain> MEMBER_MAP = new HashMap<>();
	
	@PostConstruct
	private void allBook() {
		BookDomain book1 = new BookDomain(1001, "Java & Spring Boots" , 1);
		BOOK_MAP.put(1, book1);
	}
	
	@PostConstruct
	private void allMember() {
		MemberDomain member1 = new MemberDomain((long) 640001, "test" , "test", "test");
		MEMBER_MAP.put((long) 640001, member1);
	}
	
	private static AtomicInteger counter = new AtomicInteger(640001);
	
	public void registertMember(String firstname, String lastname, String sex) {
		for (Entry<Long, MemberDomain> entry : MEMBER_MAP.entrySet()) {
			long key = entry.getKey();
			long i = counter.incrementAndGet();
			MemberDomain domain = new MemberDomain(i, firstname, lastname, sex);
//			MemberDomain val = entry.getValue();
			if(i == key) {
				i = i + 1;
			}
			else if(i != key) {
				MEMBER_MAP.put(i, domain);
				break;
			}
		}
	}
	
	public List<MemberDomain> getAllMember(){
		List<MemberDomain> list = new ArrayList<>();
		MEMBER_MAP.entrySet().stream().forEach( domain -> {
			list.add(domain.getValue());
		});
		return list;
	}
	
	public List<BookDomain> getAllBook(){
		List<BookDomain> list = new ArrayList<>();
		BOOK_MAP.entrySet().stream().forEach( member1 -> {
			list.add(member1.getValue());
		});
		return list;
	}
	
//	public void 
	
}
