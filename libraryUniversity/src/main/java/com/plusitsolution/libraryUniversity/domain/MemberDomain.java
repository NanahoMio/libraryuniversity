package com.plusitsolution.libraryUniversity.domain;

public class MemberDomain {
	private Long memberId;
	private String firstname;
	private String lastname;
	private String sex;
	
	public MemberDomain(Long memberId, String firstname, String lastname, String sex) {
		this.memberId = memberId;
		this.firstname = firstname;
		this.lastname = lastname;
		this.sex = sex;
		
	}
	public MemberDomain() {
		
	}
	public Long getMemberId() {
		return memberId;
	}
	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
}