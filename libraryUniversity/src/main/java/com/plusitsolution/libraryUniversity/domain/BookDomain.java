package com.plusitsolution.libraryUniversity.domain;

public class BookDomain {
	private Integer bookId;
	private String bookName;
	private Integer bookAmount;
	
	public BookDomain(Integer bookId, String bookName, Integer bookAmount) {
		this.bookId = bookId;
		this.bookName = bookName;
		this.bookAmount = bookAmount;
	}
	public Integer getBookId() {
		return bookId;
	}
	public void setBookId(Integer bookId) {
		this.bookId = bookId;
	}
	public String getBookName() {
		return bookName;
	}
	public void setBookName(String bookName) {
		this.bookName = bookName;
	}
	public Integer getBookAmount() {
		return bookAmount;
	}
	public void setBookAmount(Integer bookAmount) {
		this.bookAmount = bookAmount;
	}
}
