package com.plusitsolution.libraryUniversity.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.plusitsolution.libraryUniversity.domain.BookDomain;
import com.plusitsolution.libraryUniversity.domain.MemberDomain;
import com.plusitsolution.libraryUniversity.service.LibraryService;

@RestController
public class LibraryUniversityController {
	
	@Autowired
	private LibraryService service;
	
	@PostMapping("/registertMember")
	public void registertMember(@RequestParam("firstname") String firstname, @RequestParam("lastname") String lastname, @RequestParam("sex") String sex) {
		service.registertMember(firstname, lastname, sex);
	}
	
	@GetMapping("/allMember")
	public List<MemberDomain> getMemberList(){
		return service.getAllMember();
	}
	
	@GetMapping("/allBook")
	public List<BookDomain> getBookList(){
		return service.getAllBook();
	}
	
}
